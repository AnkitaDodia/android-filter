package com.example.my7.androidfilter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.library.AbstractWheel;
import com.example.library.adapters.ArrayWheelAdapter;
import com.example.library.adapters.NumericWheelAdapter;

public class MainActivity extends AppCompatActivity {

    AbstractWheel hours1,hours2,hours3,hours4,hours5,hours6,hours7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hours1 = (AbstractWheel) findViewById(R.id.hour_horizontal_1);
        hours2 = (AbstractWheel) findViewById(R.id.hour_horizontal_2);
        hours3 = (AbstractWheel) findViewById(R.id.hour_horizontal_3);
        hours4 = (AbstractWheel) findViewById(R.id.hour_horizontal_4);
        hours5 = (AbstractWheel) findViewById(R.id.hour_horizontal_5);
        hours6 = (AbstractWheel) findViewById(R.id.hour_horizontal_6);
        hours7 = (AbstractWheel) findViewById(R.id.hour_horizontal_7);

        hours2.setCyclic(true);
        hours3.setCyclic(true);
        hours4.setCyclic(true);
        hours5.setCyclic(true);

        ArrayWheelAdapter<String> ampmAdapter1 =
                new ArrayWheelAdapter<String>(this, new String[] {"man", "woman"});
        ampmAdapter1.setItemResource(R.layout.wheel_text_centered);
        ampmAdapter1.setItemTextResource(R.id.text);
        hours1.setViewAdapter(ampmAdapter1);

        NumericWheelAdapter ampmAdapter2 = new NumericWheelAdapter(this, 0, 23, "%02d");
        ampmAdapter2.setItemResource(R.layout.wheel_text_centered);
        ampmAdapter2.setItemTextResource(R.id.text);
        hours2.setViewAdapter(ampmAdapter2);

        NumericWheelAdapter ampmAdapter3 = new NumericWheelAdapter(this, 10, 50, "%02d");
        ampmAdapter3.setItemResource(R.layout.wheel_text_centered);
        ampmAdapter3.setItemTextResource(R.id.text);
        hours3.setViewAdapter(ampmAdapter3);

        ArrayWheelAdapter<String> ampmAdapter4 =
                new ArrayWheelAdapter<String>(this, new String[] {"Bangladesh", "Barbados", "Belarus", "Belgium"});
        ampmAdapter4.setItemResource(R.layout.wheel_text_centered);
        ampmAdapter4.setItemTextResource(R.id.text);
        hours4.setViewAdapter(ampmAdapter4);

        ArrayWheelAdapter<String> ampmAdapter5 =
                new ArrayWheelAdapter<String>(this, new String[] {"Bathsheba", "Hoteltown", "Oistins", "Minsk", "Mogilev"});
        ampmAdapter5.setItemResource(R.layout.wheel_text_centered);
        ampmAdapter5.setItemTextResource(R.id.text);
        hours5.setViewAdapter(ampmAdapter5);

        ArrayWheelAdapter<String> ampmAdapter6 =
                new ArrayWheelAdapter<String>(this, new String[] {"all", "online"});
        ampmAdapter6.setItemResource(R.layout.wheel_text_centered);
        ampmAdapter6.setItemTextResource(R.id.text);
        hours6.setViewAdapter(ampmAdapter6);

        ArrayWheelAdapter<String> ampmAdapter7 =
                new ArrayWheelAdapter<String>(this, new String[] {"all", "online"});
        ampmAdapter7.setItemResource(R.layout.wheel_text_centered);
        ampmAdapter7.setItemTextResource(R.id.text);
        hours7.setViewAdapter(ampmAdapter7);
    }
}
